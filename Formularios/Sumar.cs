﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Formularios
{
    public partial class Sumar : Form
    {
        public Sumar()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int valor1 =int.Parse( InputText1.Text);
            int valor2 = int.Parse(InputText2.Text);
            int resultado = valor1 + valor2;
            MessageBox.Show($"El valor del resultado es: {resultado}");

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            MenuPrincipal m = new();
            m.Show();
            this.Hide();
        }

        private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            InputText1.Text = "";
            InputText2.Text = "";
        }
    }
}
